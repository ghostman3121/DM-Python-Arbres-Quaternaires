# Exercice : Arbre quaternaire

Exercice de création, de manipulation et d'affichage d'arbres quaternaires via Python et la bibliothèque Tkinter.

## Fonctionnalités : 
- [x] Présence d'un fichier requirements.txt
- [x] Implémentation de la méthode depth()
- [x] Implémentation de la méthode from_list()
- [x] Implémentation de la méthode fromFile()
- [x] Implémentation de la méthode TkQuadTree.paint()
