"""Tests du module quadtree"""

import os
import sys

from src import QuadTree, TkQuadTree


sys.path.append(os.path.realpath(os.path.dirname(__file__) + "/.."))


def test_depth_one():
    """
    The test_depth_one function tests the depth of a QuadTree with one node.
    The expected result is 1.

    :return:None
    """
    q = QuadTree(False, True, True, True)
    assert q.depth == 1


def test_depth_max():
    """
    The test_depth_max function tests the depth of a QuadTree.
    The test_depth_max function then asserts that the depth is equal to 3.

    :return: None
    """
    qtree = QuadTree(
        QuadTree(
            QuadTree(False, False, False, False),
            True,
            False,
            True
        ),
        QuadTree(True, True, True, True),
        True,
        True
    )
    assert qtree.depth == 3


def test_from_list_easy():
    """
    The test_from_list_easy function tests the from_list function with a list that is only one level deep.
    The test_from_list_easy function creates a QuadTree object using the from_list method, and then checks that
    its depth is 1.

    :return: None
    """
    test_list = [0, 1, 0, 1]
    q = QuadTree.from_list(test_list)
    assert q.depth == 1


def test_from_list_hard():
    """
    The test_from_list_hard function tests the from_list function with a list of lists that is 4 levels deep.
    The test_from_list_hard function creates a QuadTree object using the from_list method and then checks to see if
    the depth of the tree is equal to 4.

    :return: None
    """
    test_list = [
        [0, 0, 0, [1, 0, 0, 0]],
        [0, 0, [0, 1, 0, 0], 0],
        [0, 0, 0, [[1, 0, 0, 1], [0, 0, 1, 1], 0, 0]],
        [0, 0, [[0, 0, 1, 1], [0, 1, 1, 0], 0, 0], 0]
    ]
    q = QuadTree.from_list(test_list)
    assert q.depth == 4


def test_sample():
    """
    The test_sample function tests the QuadTree.from_file function with a file containing test representing multiple
    nodes, and checks to see if the depth of the tree is 1.

    :return: None
    """
    filename = "../files/quadtree.txt"
    q = QuadTree.from_file(filename)
    assert q.depth == 4


def test_single():
    """
    The test_single function tests the QuadTree.from_file function with a file that has only one node,
    and checks to see if the depth of the tree is 1.

    :return: None
    """
    filename = "../files/quadtree_easy.txt"
    q = QuadTree.from_file(filename)
    assert q.depth == 1


def test_paint():
    """
    The test_paint function tests the paint function of the TkQuadTree class by checking if the number of generated
    squares is equal to 9.

    :return: None
    """
    assert TkQuadTree(TkQuadTree(True, True, False, False), True, False, False).paint() == 9
