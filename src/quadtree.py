""" Creation and manipulation of quadtrees

Classes:
    QuadTree
        Create quadtree from list or file, and defile its characteristics
    TkQuadTree
        Use Tkinter to draw specified quadtree pictorially
"""

from __future__ import annotations
import tkinter as tk


class QuadTree:
    """ Quaternary tree with 4 nodes that can contain an other quadtree or a boolean on each node.

    :param up_left: Represents the upper left node of the quadtree
    :param up_right: Represents the upper right node of the quadtree
    :param down_right: Represents the bottom right node of the quadtree
    :param down_left: Represents the bottom left node of the quadtree
    """
    def __init__(
            self,
            up_left: bool | QuadTree,
            up_right: bool | QuadTree,
            down_right: bool | QuadTree,
            down_left: bool | QuadTree):
        self.up_left = up_left
        self.up_right = up_right
        self.down_right = down_right
        self.down_left = down_left
        self.dict = {
            "up_left": self.up_left,
            "up_right": self.up_right,
            "down_left": self.down_left,
            "down_right": down_right}

    @property
    def depth(self) -> int:
        """ Recursion depth of the quadtree
        :return: int
        """
        max_depth = 0
        for name_attribute in self.dict:
            if isinstance(self.dict[name_attribute], QuadTree):
                max_depth = max(max_depth, self.dict[name_attribute].depth)
        return max_depth + 1

    @staticmethod
    def from_list(data: list) -> QuadTree:
        """ Generates a Quadtree from a list representation
        :param data: list
        :return: QuadTree
        """
        q = QuadTree(True, True, True, True)
        for element, name_attribute in zip(data, q.dict):
            if type(element) is list:
                q.dict[name_attribute] = QuadTree.from_list(element)
            else:
                q.dict[name_attribute] = element
        return q

    @staticmethod
    def from_file(filename: str) -> QuadTree:
        """ Open a given file, containing a textual representation of a list
        :param filename: str
        :return: QuadTree
        """
        with open(filename, "r") as file:
            file_content = file.read()
        return QuadTree.from_list(eval(file_content))


class TkQuadTree(QuadTree):
    """ TkQuadTree herits of the QuadTree class and gives with its function paint() a graphical representation
    of a quadtree.

    :param size: The size of the painted canvas
    :param number_square: The number of squares that are drawn. For debug and test purpose
    """

    def __init__(self, up_left: bool | TkQuadTree, up_right: bool | TkQuadTree, down_right: bool | TkQuadTree,
                 down_left: bool | TkQuadTree):
        super().__init__(up_left, up_right, down_right, down_left)
        self.size: int = 512
        self.number_squares: int = 0

    def paint(self):
        """
        The paint function creates a TK representation of the Quadtree.
        It takes in no arguments and returns the number of squares created.

        :param self: Represent the instance of the object itself
        :return: The number of squares that were created
        """
        root = tk.Tk()
        canvas = tk.Canvas(root, width=self.size, height=self.size)
        self.__create_squares(canvas, 0, 0, self.size, self.size)
        self.number_squares += 1
        canvas.pack()
        root.mainloop()
        return self.number_squares

    def __create_squares(
            self,
            canvas: tk.Canvas,
            array_up_left_x: int,
            array_up_left_y: int,
            array_down_right_x: int,
            array_down_right_y: int) -> None:
        """
        The __create_squares function is a recursive function that creates the squares of the quadtree.
        It takes in a canvas, and four integers representing the up left x coordinate, up left y coordinate,
        down right x coordinate and down right y coordinate. It then uses these coordinates to create rectangles
        on the canvas for each square in our quadtree.

        :param self: Access the attributes and methods of the class
        :param canvas: tk.Canvas: Draw the squares on the canvas
        :param array_up_left_x: int: Define the x coordinate of the upper left corner of the array
        :param array_up_left_y: int: Set the y coordinate of the upper left corner of the array
        :param array_down_right_x: int: Specify the x coordinate of the bottom right corner of the array
        :param array_down_right_y: int: Create the down right y coordinate of the array
        :return: None
        """
        up_left_x: int = 0
        up_left_y: int = 0
        down_right_x: int = 0
        down_right_y: int = 0
        for name_attribute in self.dict:
            match name_attribute:
                case "up_left":
                    up_left_x = array_up_left_x
                    up_left_y = array_up_left_y
                    down_right_x = ((array_down_right_x - array_up_left_x) // 2) + array_up_left_x
                    down_right_y = ((array_down_right_y - array_up_left_y) // 2) + array_up_left_y
                case "up_right":
                    up_left_x = ((array_down_right_x - array_up_left_x) // 2) + array_up_left_x
                    up_left_y = array_up_left_y
                    down_right_x = array_down_right_x
                    down_right_y = ((array_down_right_y - array_up_left_y) // 2) + array_up_left_y
                case "down_right":
                    up_left_x = ((array_down_right_x - array_up_left_x) // 2) + array_up_left_x
                    up_left_y = ((array_down_right_y - array_up_left_y) // 2) + array_up_left_y
                    down_right_x = array_down_right_x
                    down_right_y = array_down_right_y
                case "down_left":
                    up_left_x = array_up_left_x
                    up_left_y = ((array_down_right_y - array_up_left_y) // 2) + array_up_left_y
                    down_right_x = ((array_down_right_x - array_up_left_x) // 2) + array_up_left_x
                    down_right_y = array_down_right_x
            if isinstance(self.dict[name_attribute], TkQuadTree):
                canvas.create_rectangle(up_left_x, up_left_y, down_right_x, down_right_y, outline="grey", fill="white")
                self.dict[name_attribute].__create_squares(
                    canvas,
                    up_left_x,
                    up_left_y,
                    down_right_x,
                    down_right_y)
                self.number_squares += self.dict[name_attribute].number_squares
            else:
                if self.dict[name_attribute]:
                    canvas.create_rectangle(
                        up_left_x,
                        up_left_y,
                        down_right_x,
                        down_right_y,
                        outline="grey",
                        fill="black")
                else:
                    canvas.create_rectangle(
                        up_left_x,
                        up_left_y,
                        down_right_x,
                        down_right_y,
                        outline="grey",
                        fill="white")
            self.number_squares += 1
